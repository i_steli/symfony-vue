export function getCurrentCategoryId() {
    return window.currentCategoryId;
}

export function getCurrentProductId() {
    return window.currentProductId;
}
