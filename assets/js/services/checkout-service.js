import axios from 'axios';

export function createOrder(data) {
    return axios.post('/api/purchases', data);
}
