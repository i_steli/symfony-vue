export default (price) => (price / 100)
    .toLocaleString('en-Us', { minimumFractionDigits: 2 });
