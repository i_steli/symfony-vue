import Vue from 'vue';
import App from '@/pages/shopping-cart';
// import {createApp} from 'vue' in vue 3

new Vue({
    render: (h) => h(App),
}).$mount('#app');
